# Ical

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `ical` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:ical, "~> 0.1.0"}]
    end
    ```

  2. Ensure `ical` is started before your application:

    ```elixir
    def application do
      [applications: [:ical]]
    end
    ```

